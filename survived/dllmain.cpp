// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"

#include "interface.h"
#include "IGameConsole.h"
#include "IVEngineClient.h"

#include <MinHook.h>
#include <Windows.h>
#include <io.h>
#include <stdio.h>
#include <fcntl.h>
#include <process.h>

typedef HMODULE (WINAPI *LOADLIBRARYEX)(LPCSTR, HANDLE, DWORD);
typedef HMODULE (WINAPI *LOADLIBRARY)(LPCSTR);

// Pointers
LOADLIBRARYEX fpLoadLibraryEx = NULL;
LOADLIBRARY fpLoadLibrary = NULL;
CreateInterfaceFn fpGameUIFactory = NULL;

//IGameConsole *console = NULL;
IVEngineClient *client = NULL;

class Console : IGameConsole
{
	void Activate()
	{
		//MessageBox( NULL, "Activate", "GameConsole", 0 );
	}

	void Initialize()
	{
		//MessageBox( NULL, "Initialize", "GameConsole", 0 );
	}

	// hides the console
	void Hide()
	{
		//MessageBox( NULL, "Hide", "GameConsole", 0 );
	}

	// clears the console
	void Clear()
	{
		//MessageBox( NULL, "Clear", "GameConsole", 0 );
	}

	// return true if the console has focus
	bool IsConsoleVisible()
	{
		//MessageBox( NULL, "IsConsoleVisible", "GameConsole", 0 );
		return false;
	}

	// prints a message to the console
	void Printf(const char *format, ...)
	{
		//MessageBox( NULL, "Printf", "GameConsole", 0 );
		va_list args;
		va_start(args, format);
		vprintf( format, args );
		va_end(args);
	}

	// printes a debug message to the console
	void DPrintf(const char *format, ...)
	{
		//MessageBox( NULL, "DPrintf", "GameConsole", 0 );
		va_list args;
		va_start(args, format);
		vprintf( format, args );
		va_end(args);
	}

	// printes a debug message to the console
	void ColorPrintf( Color& clr, const char *format, ...)
	{
		//MessageBox( NULL, "ColorPrintf", "GameConsole", 0 );
		va_list args;
		va_start(args, format);
		vprintf( format, args );
		va_end(args);
	}

	void SetParent( int parent )
	{
		//MessageBox( NULL, "SetParent", "GameConsole", 0 );
	}
};

void DisplayError()
{
	LPTSTR errorText = NULL;

	FormatMessage(
	   // use system message tables to retrieve error text
	   FORMAT_MESSAGE_FROM_SYSTEM
	   // allocate buffer on local heap for error text
	   |FORMAT_MESSAGE_ALLOCATE_BUFFER
	   // Important! will fail otherwise, since we're not 
	   // (and CANNOT) pass insertion parameters
	   |FORMAT_MESSAGE_IGNORE_INSERTS,  
	   NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
	   GetLastError(),
	   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
	   (LPTSTR)&errorText,  // output 
	   0, // minimum size for output buffer
	   NULL);   // arguments - see note 

	if ( NULL != errorText )
	{
	   MessageBox( NULL, errorText, errorText, 0 );
	   LocalFree(errorText);
	   errorText = NULL;
	}
	else
	{
		MessageBox( NULL, "DisplayError", "DisplayError", 0 );
	}
}

// Detour function which overrides LoadLibraryEx.
HMODULE WINAPI DetourLoadLibraryEx( LPCSTR lpLibFileName, HANDLE hFile, DWORD dwFlags )
{
	//MessageBox( NULL, lpLibFileName, "LoadLibraryEx", 0 );
	HMODULE mod = fpLoadLibraryEx( lpLibFileName, hFile, dwFlags );

	return mod;
}

void *DetourGameUIFactory( const char *pName, int *pReturnCod )
{
	if( strcmp( pName, GAMECONSOLE_INTERFACE_VERSION ) == 0 )
	{
		//MessageBox( NULL, "gameconsole get", "gameconsole get", 0 );
		return new Console();
	}

	return fpGameUIFactory( pName, pReturnCod );
}

void InputThread(void* param)
{
	char inbuf[1024];
	memset( inbuf, 0, 1024 );
	int loc = 0;
	while(true)
	{
		char c = getchar();
		inbuf[loc] = c;
		loc++;
		putchar(c);
		if( c == '\n' )
		{
			//MessageBox( NULL, inbuf, "Command Entered", 0 );
			client->ClientCmd( inbuf );
			loc = 0;
			memset( inbuf, 0, 1024 );
		}
	}
}

// Detour function which overrides LoadLibrary.
HMODULE WINAPI DetourLoadLibrary( LPCSTR lpLibFileName )
{
	//MessageBox( NULL, lpLibFileName, "LoadLibrary", 0 );
	// We need to watch out for the gameui call
	if( strstr( lpLibFileName, "gameui" ) != NULL)
	{
		// Wee
		//MessageBox( NULL, "gameui Loaded", "gameui Loaded", 0 );

		AllocConsole();
		SetConsoleTitle("Debug Console");
		HANDLE conOut = GetStdHandle( STD_OUTPUT_HANDLE );
		int conOutS = _open_osfhandle( (intptr_t)conOut, _O_TEXT );
		*stdout = *_fdopen( conOutS, "w" );
		setvbuf( stdout, NULL, _IONBF, 0 );

		HANDLE conIn = GetStdHandle( STD_INPUT_HANDLE );
		int conInS = _open_osfhandle( (intptr_t)conIn, _O_TEXT );
		*stdin = *_fdopen( conInS, "r" );
		setvbuf( stdin, NULL, _IONBF, 0 );

		_beginthread( InputThread, 0, NULL );

		HMODULE gameui = fpLoadLibrary("gameui.dll");
		if( gameui == NULL )
		{
			DisplayError();
			goto failed;
		}

		FARPROC gameUIFactory = GetProcAddress(gameui, "CreateInterface" );
		if( gameUIFactory == NULL )
		{
			DisplayError();
			goto failed;
		}

		MH_STATUS stat = MH_CreateHook(gameUIFactory, &DetourGameUIFactory, reinterpret_cast<void**>(&fpGameUIFactory));
		// Create a hook for CreateInterface, in disabled state.
		if (stat != MH_OK)
		{
			MessageBox( NULL, "GAMEUI HOOK CREATE FAILED", "", 0 );
			goto failed;
		}

		// Enable the hook for CreateInterface.
		if (MH_EnableHook(gameUIFactory) != MH_OK)
		{
			MessageBox( NULL, "GAMEUI HOOK ACTIVATE FAILED", "", 0 );
			goto failed;
		}

		HMODULE engine = fpLoadLibrary("engine.dll");
		CreateInterfaceFn engineFactory = (CreateInterfaceFn)GetProcAddress(engine, "CreateInterface" );
		client = (IVEngineClient *)engineFactory(VENGINE_CLIENT_INTERFACE_VERSION, 0);

		/*console = (IGameConsole *)gameUIFactory( GAMECONSOLE_INTERFACE_VERSION, NULL );
		if( console != NULL )
		{
			console->Initialize();
			console->Activate();
			if( !console->IsConsoleVisible() )
				MessageBox( NULL, "Console not visible", "Console not visible", 0 );
		}
		else
			MessageBox( NULL, "Console failed", "Console failed", 0 );*/
	}

	failed:

	//MessageBox( NULL, lpLibFileName, "LoadLibrary", 0 );
	HMODULE mod = fpLoadLibrary( lpLibFileName );

	return mod;
}

void SetupDetour()
{
	/*// Load the libraries that we need to detour
	if( LoadLibrary( "nesys.dll" ) == NULL )
		DisplayError();

	if( LoadLibrary( "ac.dll" ) == NULL )
		DisplayError();

	MessageBox( NULL, "Libraries loaded!", "Libraries loaded!", 0 );*/

	// Initialize MinHook.
    if (MH_Initialize() != MH_OK)
    {
        return;
    }

    // Create a hook for LoadLibraryEx, in disabled state.
    if (MH_CreateHook(&LoadLibraryExA, &DetourLoadLibraryEx, 
	reinterpret_cast<void**>(&fpLoadLibraryEx)) != MH_OK)
    {
        return;
    }

    // Enable the hook for LoadLibraryEx.
    if (MH_EnableHook(&LoadLibraryExA) != MH_OK)
    {
        return;
    }

    // Create a hook for LoadLibraryEx, in disabled state.
    if (MH_CreateHook(&LoadLibraryA, &DetourLoadLibrary, 
	reinterpret_cast<void**>(&fpLoadLibrary)) != MH_OK)
    {
        return;
    }

    // Enable the hook for LoadLibraryEx.
    if (MH_EnableHook(&LoadLibraryA) != MH_OK)
    {
        return;
    }
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		SetupDetour();
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

